from django.db import models

# Create your models here.

class Jadwal(models.Model):
    nama_kegiatan = models.CharField(max_length=30)
    tempat = models.CharField(max_length=30)
    kategori = models.CharField(max_length=30)
    waktu = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    
class Account(models.Model):
    nama = models.CharField(max_length=30)
    email = models.CharField(max_length=30)
    password = models.CharField(max_length=30)