from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from .forms import FormJadwal
from .models import Jadwal, Account
import datetime
import requests
from django.http import HttpResponse
from django.core import serializers
import json
from django.views.decorators.csrf import csrf_protect
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.contrib.auth import logout
# Create your views here.

def hello(request):
    return HttpResponse ("Hello wolrd!")

def index(request):
    return render(request,'index.html')

def blog(request):
    return render(request,'blog.html')

def form(request):
    return render(request,'form.html')

@csrf_protect
def subscribe(request):
    if request.method == 'POST':
        data = request.POST.copy()
        nama = data.get('nama')
        email = data.get('email')
        password = data.get('password')

        s = Account.objects.create(
            nama=nama,
            email=email,
            password=password
        )
        s.save()
        response = HttpResponse(status=200)
        return render(request,'subscribe.html', response)
        
    elif request.method == 'DELETE':
        email = request.DELETE.get('email')
        Account.objects.get(email=email).delete()
        return render(request,'subscribe.html')


    else:
        return render(request,'subscribe.html')    

def daftar_subscriber(request):
    subs = Account.objects.all()
    data = serializers.serialize('json', subs)
    return HttpResponse(data, content_type='application/json')

def unsub(request):
    email = request.POST.get('email')
    Account.objects.get(email=email).delete()
    return render(request,'subscribe.html')


def jadwal(request):
    form = FormJadwal(request.POST)
    jadwal = Jadwal.objects.all().values()
    return render(request, 'jadwal.html', {'form':form, 'jadwal':jadwal})
    
def books(request):
    request.session['fav'] = 0

    if request.method == 'POST':
        data = request.POST.copy()
        query = data.get('query')
        url = 'https://www.googleapis.com/books/v1/volumes?q=%s' % query
        res = requests.get(url)
        data = res.json()
        books = data['items']
        print(url)
        return render(request,'books.html', {'books':books})
        
    else:
        url = 'https://www.googleapis.com/books/v1/volumes?q=quilting'
        res = requests.get(url)
        data = res.json()
        books = data['items']
        return render(request,'books.html', {'books':books})

def check_email(request):
    email = request.GET['email']

    if(Account.objects.filter(email=email).count()):
        return  HttpResponse(True)

    return  HttpResponse(False)

def check_password(request):
    email = request.GET['email']
    password = request.GET['password']

    obj = Account.objects.get(email=email)
    if password == obj.password:
        return HttpResponse(True)
    else:
        return HttpResponse(False)



def add_jadwal(request):
    if request.method == 'POST':
        data = request.POST.copy()
        nama_kegiatan = data.get('nama_kegiatan')
        tempat = data.get('tempat')
        kategori = data.get('kategori')
        waktu = data.get('waktu')

        j = Jadwal.objects.create(
            nama_kegiatan=nama_kegiatan,
            tempat = tempat,
            kategori = kategori,
            waktu = waktu)
        j.save()

    return HttpResponseRedirect('/index/jadwal')

def delete_all_jadwal(request):
    if request.method == 'POST':
        query = Jadwal.objects.all().delete()

    return HttpResponseRedirect('/index/jadwal')


def login(request):
    return render(request, 'login.html')

def signout(request):
    logout(request)
    return HttpResponseRedirect('/index/books')


def verifikasi(request):
    return render(request, 'google17b56790938cb27b.html')

        
