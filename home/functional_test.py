from django.test import TestCase
import unittest
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import datetime
import time
from .models import Jadwal

class CreateJadwalTest(unittest.TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium = webdriver.Chrome()
        super(CreateJadwalTest, self).setUp()

    def tearDown(self):
        self.selenium.implicitly_wait(3)
        self.selenium.quit()
        super(CreateJadwalTest, self).tearDown()

    def create(self):
        browser = self.selenium
        #Opening the link we want to test
        browser.get('http://127.0.0.1:8000/index/jadwal/')

        # Find the "Add Jadwal" Button to open up the forms in a modal
        add_jadwal = browser.find_element_by_id('add_jadwal')
        add_jadwal.send_keys(Keys.RETURN)

        # Find the required fields in forms
        nama_kegiatan = browser.find_element_by_name('nama_kegiatan')
        tempat = browser.find_element_by_name('tempat')
        kategori = browser.find_element_by_name('kategori')
        waktu = browser.find_element_by_name('waktu')

        # Fill the fields
        nama_kegiatan.send_keys('Belajar PPW')
        tempat.send_keys('Lab Komputer Fasilkom')
        kategori.send_keys('Pendidikan')
        waktu.send_keys(datetime.datetime.now())

        # Submit Form
        submit = browser.find_element_by_id('submit')
        submit.send_keys(Keys.RETURN)

if __name__ == '__main__':
    unittest.main()


