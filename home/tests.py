import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import datetime
import time
from .models import Jadwal

class CreateJadwalTest(unittest.TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        # chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        # self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.driver = webdriver.Chrome()

    def test_create_new_jadwal(self):
        driver = self.driver

        # Go to the link we want to test
        driver.get("http://127.0.0.1:8000/index/jadwal/")

        # Check if the page lay out is correct
        self.assertIn("Hardlexander's Personal Website", driver.title)
        header = driver.find_element_by_id('home')
        self.assertEqual(header.get_attribute('class'), 'header')
        
        # Check if CSS is correct
        css = driver.find_elements_by_xpath("//link[@type='text/css']")
        self.assertIn("bootstrap.css", css[0].get_attribute('href'))
        self.assertIn("style.css", css[1].get_attribute('href'))

        # Find the "Add Jadwal" Button to open up the forms in a modal
        add_jadwal = driver.find_element_by_id('add_jadwal')
        add_jadwal.send_keys(Keys.RETURN)

        # Find the required fields in forms
        nama_kegiatan = driver.find_element_by_xpath("//input[@name='nama_kegiatan']")
        tempat = driver.find_element_by_xpath("//input[@name='tempat']")
        kategori = driver.find_element_by_xpath("//input[@name='kategori']")
        waktu = driver.find_element_by_xpath("//input[@name='waktu']")

        # Fill the fields
        nama_kegiatan.send_keys('Belajar PPW')
        tempat.send_keys('Lab Komputer Fasilkom')
        kategori.send_keys('Pendidikan')
        waktu.send_keys('01012011')
        waktu.send_keys(Keys.TAB)
        waktu.send_keys('0245PM')

        # Submit Form
        submit = driver.find_element_by_id('submit')
        submit.send_keys(Keys.RETURN)

        # Check if Jadwal is created
        check_nama_kegiatan = driver.find_element_by_xpath("//h3[@name='nama_kegiatan']").text
        check_tempat = driver.find_element_by_xpath("//li[@name='tempat']").text
        check_kategori = driver.find_element_by_xpath("//li[@name='kategori']").text

        self.assertEqual(check_nama_kegiatan, 'BELAJAR PPW - JAN. 1, 2011, 2:45 P.M.')
        self.assertEqual(check_tempat, 'Tempat: Lab Komputer Fasilkom')
        self.assertEqual(check_kategori, 'Kategori: Pendidikan')

        # time.sleep(5)

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()

# source: https://selenium-python.readthedocs.io