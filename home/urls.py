from . import views
from django.urls import path
from django.conf.urls import include
from django.conf import settings
from django.contrib.auth import logout

urlpatterns = [
    path('hello/', views.hello),
    path('', views.index),
    path('blog/', views.blog),
    path('form/', views.form),
    path('jadwal/', views.jadwal),
    path('add_jadwal/', views.add_jadwal),
    path('delete_all_jadwal/', views.delete_all_jadwal),
    path('books/', views.books),
    path('check_email/', views.check_email),
    path('subscribe/', views.subscribe),
    path('daftar_subscriber/', views.daftar_subscriber),
    path('unsubscribe/', views.unsub),
    path('check_password/', views.check_password),
    path('login/', views.login),
    path('', include('social_django.urls', namespace='social')),
    path('logout/', views.signout, name='logout'),
]