from django.contrib import admin
from .models import Jadwal, Account

# Register your models here.
admin.site.register(Jadwal)
admin.site.register(Account)