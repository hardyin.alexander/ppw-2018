from django import forms
import datetime

class FormJadwal(forms.Form):
    kegiatan = forms.CharField(
        required=True,
        label='',
        widget=forms.TextInput(attrs={
            'type': "text",
            'value': "Nama Kegiatan",
            'onfocus':"this.value = '';",
            'onblur':"if (this.value == '') {this.value = 'Nama Kegiatan';}"
            })
        )
    tempat = forms.CharField(
        required=True,
        label='',
        widget=forms.TextInput(attrs={
            'type': "text",
            'value': "Tempat",
            'onfocus':"this.value = '';",
            'onblur':"if (this.value == '') {this.value = 'Tempat';}"
            })
        )
    kategori = forms.CharField(
        required=True,
        label='',
        widget=forms.TextInput(attrs={
            'type': "text",
            'value': "Kategori",
            'onfocus':"this.value = '';",
            'onblur':"if (this.value == '') {this.value = 'Kategori';}",
            'required':"true"
            })
        )

    waktu = forms.DateTimeField(
        label='',
        initial=datetime.datetime.now(),
        widget=forms.DateTimeInput(attrs={ 
            'type': "datetime-local",
            'value': "Waktu",
            'onfocus':"this.value = '';",
            'onblur':"if (this.value == '') {this.value = 'Waktu';}",
            'required':"true"
            })
    )